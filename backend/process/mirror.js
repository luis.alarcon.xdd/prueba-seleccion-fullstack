const request = require('request');
const Character = require('../models/character');

// Main, proceso que realiza el copiado de los datos
// si detecta la misma cantidad de datos no realiza accion, si detecta un numero mayor en la api copiara los nuevos registros
let mirrorData = async() => {

    let dataAPI = await getAPIData();
    let dataDB = await getDBData();

    if (dataAPI.length !== dataDB) {
        for (let index = dataDB; index < dataAPI.length; index++) {
            await addData(dataAPI[index]);
        }
        console.log('complete');
    } else {
        console.log('complete');
    }


}

// funcion que trae todos los datos de la api
let getAPIData = async() => {

    return new Promise((resolve, reject) => {

        let options = {
            'method': 'GET',
            'url': `${process.env.API}/show/characters`,
            'headers': {}
        };
        request(options, function(error, response) {
            if (error) {
                console.log(error);
                reject(error)
                return
            }
            resolve(JSON.parse(response.body));
            return
        });
    })


}

// funcion para traer los registros de la db
let getDBData = async() => {

    let result = Character.countDocuments({}, (err, conteo) => {
        return conteo;
    });
    return result;
}

// funcion para agregar los datos a la db
let addData = async(person) => {

    let character = new Character({
        titles: person.titles,
        origin: person.origin,
        siblings: person.siblings,
        spouse: person.spouse,
        lovers: person.lovers,
        plod: person.plod,
        longevity: person.longevity,
        plodB: person.plodB,
        plodC: person.plodC,
        longevityB: person.longevityB,
        longevityC: person.longevityC,
        culture: person.culture,
        religion: person.origreligionin,
        allegiances: person.allegiances,
        seasona: person.seasona,
        appearances: person.appearances,
        name: person.name,
        slug: person.slug,
        image: person.image,
        gender: person.gender,
        alive: person.alive,
        death: person.death,
        father: person.father,
        house: person.house,
        first_seen: person.first_seen,
        actor: person.actor,
        appearances: person.appearances,
        createAt: person.createAt,
        updateAt: person.updateAt,
        __v: person.__v,
        pagerank: person.pagerank,
        age: person.age,
        id: person.id
    });

    character.save((err, characterDB) => {
        if (err) {
            console.log(err);
        }
    })
}


module.exports = ({
    mirrorData
})
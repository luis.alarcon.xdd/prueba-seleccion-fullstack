const { Router } = require('express');

const {
    getCharacter,
    getAllCharacters
} = require('../controllers/characters');

const router = Router();

router.get('/:id', getCharacter);
router.get('/', getAllCharacters);

module.exports = router;
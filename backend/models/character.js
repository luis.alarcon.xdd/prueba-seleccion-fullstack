const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let characterSchema = new Schema({
    titles: { type: Array },
    origin: { type: Array },
    siblings: { type: Array },
    spouse: { type: Array },
    lovers: { type: Array },
    plod: { type: Number },
    longevity: { type: Array },
    plodB: { type: Number },
    plodC: { type: Number },
    longevityB: { type: Array },
    longevityC: { type: Array },
    culture: { type: Array },
    religion: { type: Array },
    allegiances: { type: Array },
    seasona: { type: Array },
    appearances: { type: Array },
    name: { type: String },
    slug: { type: String },
    image: { type: String },
    gender: { type: String },
    alive: { type: Boolean },
    death: { type: Number },
    father: { type: String },
    house: { type: String },
    first_seen: { type: String },
    actor: { type: String },
    appearances: { type: Array },
    createAt: { type: Date },
    updateAt: { type: Date },
    __v: { type: Number },
    pagerank: { type: Array },
    age: { type: Array },
    id: { type: String }
});


characterSchema.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();

    return userObject;
}


module.exports = mongoose.model('Character', characterSchema);
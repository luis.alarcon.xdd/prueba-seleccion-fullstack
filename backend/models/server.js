const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const { mirrorData } = require('../process/mirror');
var path = require('path');
var appDir = path.dirname(require.main.filename);


require('../config/config');

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.charactersPath = '/characters';
        this.publicPath = '/';

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();
    }

    middlewares() {

        // CORS
        this.app.use(cors());

        // Lectura y parseo del body
        this.app.use(express.json());

        // Directorio Público
        this.app.use(express.static('public'));

        // conexion a moongoDB
        mongoose.connect(process.env.urlDB, {
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true
        }, (err, res) => {
            if (err) throw err;
            console.log('Base de datos Online');
        });

        // cuando levante el servido y cada 10 minutos hara una sincronización de los datos

        console.log('sincronizacion inicial');
        mirrorData();


        setTimeout(async() => {
            console.log('sincronizacion');
            await mirrorData();
        }, 600000);
        // 10 minutes = 600000 

    }

    routes() {
        this.app.use(this.charactersPath, require('../routes/characters'));

        // public route
        this.app.use(express.static(appDir + '/frontend'));

    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto', this.port);
        });
    }

}


module.exports = Server;
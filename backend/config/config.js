// ===============
// puerto
// ===============

process.env.PORT = process.env.PORT || 3000;
process.env.NODE_ENV = 'dev'

// ===============
// Mongo
// ===============

let urlDB;

if (process.env.NODE_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/tactech';
} else {
    urlDB = process.env.MONGO_URI;
}

console.log(urlDB);

process.env.urlDB = urlDB;

// ===============
// endPoint
// ===============
process.env.API = process.env.API || 'https://api.got.show/api'
const { response, request } = require('express');
const Character = require('../models/character');

const getCharacter = (req = request, res = response) => {
    const { id } = req.params;

    if (!id) {
        return res.status(400).json({
            errorID: 3,
            mensaje: 'Id no valido',
            data: {}
        });
    }

    Character.find({ id })
        .exec((err, character) => {
            if (err) {
                return res.status(400).json({
                    errorID: 1,
                    mensaje: err,
                    data: {}
                });
            }
            if (!character[0]) {
                return res.status(400).json({
                    errorID: 2,
                    mensaje: 'Personaje no existe',
                    data: {}
                });
            }

            return res.json({
                errorID: 0,
                mensaje: '',
                data: character
            });
        });
}

const getAllCharacters = (req = request, res = response) => {
    let { pag } = req.query;
    const limite = 10;

    if (!pag) { pag = 0 }
    pag = Number(pag);

    Character.find({})
        .skip(pag)
        .limit(10)
        .exec((err, characters) => {
            if (err) {
                return res.status(400).json({
                    errorID: 3,
                    mensaje: err,
                    total: 0,
                    pag: 0,
                    data: {}
                });
            }

            Character.countDocuments({}, (err, conteo) => {
                res.json({
                    errorID: 0,
                    mensaje: '',
                    total: conteo,
                    pag,
                    data: { characters }
                })

            });

        })
}


module.exports = {
    getCharacter,
    getAllCharacters
}